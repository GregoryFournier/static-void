package com.stat.vod.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.stat.vod.MainApplication

    fun main(arg: Array<String>) {
        println(arg)
        val config = LwjglApplicationConfiguration()
        config.width = 1366
        config.height = 768
//        config.fullscreen = true
        LwjglApplication(MainApplication(), config)
    }
