[
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 0
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 48
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 96
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 144
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 192
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 240
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 336
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 384
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 432
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 480
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 624
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 672
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -144
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 0
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 48
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 96
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 144
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 192
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 240
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 336
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 384
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 432
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 480
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 624
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 672
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 672
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 672
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 672
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 672
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 624
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 480
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 432
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 384
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 336
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 240
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 192
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 144
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 96
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 48
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 0
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -144
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -144
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -144
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -144
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -144
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 0
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 96
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 192
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 240
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 336
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 384
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 432
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 480
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 624
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 624
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 624
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 480
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 432
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 384
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 336
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 240
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 192
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 144
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 96
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 48
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 0
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 48
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 144
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 0
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 48
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 96
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 144
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 192
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 240
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 336
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 384
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 432
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 480
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 480
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 432
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 384
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 336
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 240
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 192
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 144
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 96
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 48
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 0
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 624
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 672
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 288
	regionY: 576
	tileWidth: 240
	tileHeight: 144
	x: 144
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 240
	regionY: 48
	tileWidth: 96
	tileHeight: 240
	x: 96
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 144
	regionY: 48
	tileWidth: 96
	tileHeight: 240
	x: 0
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 144
	regionY: 48
	tileWidth: 96
	tileHeight: 240
	x: 336
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 240
	regionY: 48
	tileWidth: 96
	tileHeight: 240
	x: 432
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 288
	regionY: 48
	tileWidth: 96
	tileHeight: 48
	x: 144
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 96
	regionY: 48
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 96
	regionY: 48
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 96
	regionY: 48
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 96
	regionY: 48
	tileWidth: 48
	tileHeight: 48
	x: -144
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 96
	regionY: 48
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 96
	regionY: 48
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 96
	regionY: 48
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 96
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 96
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 96
	tileWidth: 48
	tileHeight: 48
	x: -144
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 96
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 96
	tileWidth: 48
	tileHeight: 48
	x: 192
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 96
	tileWidth: 48
	tileHeight: 48
	x: 240
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 96
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 96
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 96
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -192
	y: 48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -144
	y: 48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -96
	y: 48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -96
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -48
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -48
	y: 48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 0
	y: 48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 144
	regionY: 48
	tileWidth: 48
	tileHeight: 240
	x: 0
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 0
	regionY: 48
	tileWidth: 192
	tileHeight: 48
	x: -144
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -48
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -96
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -144
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -192
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 240
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 528
	regionY: 624
	tileWidth: 192
	tileHeight: 96
	x: 192
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS1
	regionX: 0
	regionY: 288
	tileWidth: 96
	tileHeight: 96
	x: -144
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS1
	regionX: 0
	regionY: 288
	tileWidth: 96
	tileHeight: 96
	x: -144
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS1
	regionX: 0
	regionY: 288
	tileWidth: 96
	tileHeight: 96
	x: 576
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS1
	regionX: 0
	regionY: 288
	tileWidth: 96
	tileHeight: 96
	x: 576
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS1
	regionX: 288
	regionY: 288
	tileWidth: 96
	tileHeight: 144
	x: -96
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS1
	regionX: 480
	regionY: 0
	tileWidth: 48
	tileHeight: 144
	x: -144
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 480
	y: 48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 528
	y: 48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 576
	y: 48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 624
	y: 48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 672
	y: 48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 288
	regionY: 48
	tileWidth: 48
	tileHeight: 192
	x: 480
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 528
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 576
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 624
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 672
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 0
	regionY: 48
	tileWidth: 48
	tileHeight: 192
	x: -240
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 0
	regionY: 144
	tileWidth: 48
	tileHeight: 48
	x: -240
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 0
	regionY: 144
	tileWidth: 48
	tileHeight: 48
	x: -240
	y: 48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 0
	regionY: 144
	tileWidth: 48
	tileHeight: 48
	x: -240
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 0
	regionY: 144
	tileWidth: 48
	tileHeight: 48
	x: -240
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 0
	regionY: 144
	tileWidth: 48
	tileHeight: 48
	x: -240
	y: -96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 0
	regionY: 144
	tileWidth: 48
	tileHeight: 48
	x: -240
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 0
	regionY: 144
	tileWidth: 48
	tileHeight: 48
	x: -240
	y: -192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 0
	regionY: 144
	tileWidth: 48
	tileHeight: 48
	x: -240
	y: -240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 0
	regionY: 144
	tileWidth: 48
	tileHeight: 144
	x: -240
	y: -336
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 240
	tileWidth: 96
	tileHeight: 48
	x: -192
	y: -336
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 240
	tileWidth: 96
	tileHeight: 48
	x: -96
	y: -336
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 240
	tileWidth: 96
	tileHeight: 48
	x: 0
	y: -336
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 240
	tileWidth: 96
	tileHeight: 48
	x: 96
	y: -336
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 240
	tileWidth: 96
	tileHeight: 48
	x: 192
	y: -336
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 240
	tileWidth: 96
	tileHeight: 48
	x: 288
	y: -336
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 240
	tileWidth: 96
	tileHeight: 48
	x: 384
	y: -336
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 240
	tileWidth: 96
	tileHeight: 48
	x: 480
	y: -336
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 48
	regionY: 240
	tileWidth: 96
	tileHeight: 48
	x: 576
	y: -336
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 240
	tileWidth: 96
	tileHeight: 48
	x: 672
	y: -336
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 144
	regionY: 48
	tileWidth: 48
	tileHeight: 192
	x: 720
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 144
	regionY: 96
	tileWidth: 48
	tileHeight: 144
	x: 720
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 144
	regionY: 96
	tileWidth: 48
	tileHeight: 144
	x: 720
	y: -288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 144
	regionY: 96
	tileWidth: 48
	tileHeight: 144
	x: 720
	y: -144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -144
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 0
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -144
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 0
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 48
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 96
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 144
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 192
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 240
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 336
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 384
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 432
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 480
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 624
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 672
	y: 144
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 672
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 624
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 480
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 432
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 384
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 336
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 240
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 192
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 144
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 96
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 48
	y: 192
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -144
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 0
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 48
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 96
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 144
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 192
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 240
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 336
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 384
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 432
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 480
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 624
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 672
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -144
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -96
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -48
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 0
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 48
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 96
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 144
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 192
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 240
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 288
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 336
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 384
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 432
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 480
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 528
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 576
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 624
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: 672
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 144
	regionY: 48
	tileWidth: 48
	tileHeight: 240
	x: 0
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 144
	regionY: 48
	tileWidth: 48
	tileHeight: 240
	x: 336
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 288
	regionY: 48
	tileWidth: 48
	tileHeight: 240
	x: 480
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 288
	regionY: 48
	tileWidth: 48
	tileHeight: 240
	x: 144
	y: -48
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -48
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -96
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -144
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: -192
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 192
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 240
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 288
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 528
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 576
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 624
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS2
	regionX: 336
	regionY: 48
	tileWidth: 48
	tileHeight: 96
	x: 672
	y: 96
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 240
	regionY: 96
	tileWidth: 144
	tileHeight: 96
	x: -144
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 240
	regionY: 96
	tileWidth: 144
	tileHeight: 96
	x: -96
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 240
	regionY: 96
	tileWidth: 144
	tileHeight: 96
	x: -48
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 240
	regionY: 96
	tileWidth: 144
	tileHeight: 96
	x: -48
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 240
	regionY: 96
	tileWidth: 144
	tileHeight: 96
	x: 0
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 240
	regionY: 96
	tileWidth: 144
	tileHeight: 96
	x: 48
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 240
	regionY: 96
	tileWidth: 144
	tileHeight: 96
	x: 48
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 240
	regionY: 96
	tileWidth: 144
	tileHeight: 96
	x: 96
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 96
	regionY: 192
	tileWidth: 48
	tileHeight: 48
	x: -192
	y: 0
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: -192
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: -144
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: -96
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: -48
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 0
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 48
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 96
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 144
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 192
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 240
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 288
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 336
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 384
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 432
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 480
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 528
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 576
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 624
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: WOOD1
	regionX: 192
	regionY: 48
	tileWidth: 48
	tileHeight: 144
	x: 672
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS1
	regionX: 288
	regionY: 432
	tileWidth: 96
	tileHeight: 96
	x: 240
	y: 288
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS1
	regionX: 336
	regionY: 528
	tileWidth: 192
	tileHeight: 96
	x: -96
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS1
	regionX: 336
	regionY: 528
	tileWidth: 192
	tileHeight: 96
	x: 480
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS1
	regionX: 480
	regionY: 624
	tileWidth: 48
	tileHeight: 48
	x: 48
	y: 240
}
{
	class: com.stat.vod.system.tiles.Tile
	tileset: ITEMS1
	regionX: 480
	regionY: 624
	tileWidth: 48
	tileHeight: 48
	x: 624
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: -336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: -288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: -240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: -192
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: -144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: -96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: -48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: 0
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: 192
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -240
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -192
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -192
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -192
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -144
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -96
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -48
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 0
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 48
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 96
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 144
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 192
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 240
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 288
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 336
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 384
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 432
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 480
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 528
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 576
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 624
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 672
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 672
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 672
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 624
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 576
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 528
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 480
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 432
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 384
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 336
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 288
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 240
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 192
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 144
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 96
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 48
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 0
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -48
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -96
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -144
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -144
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -96
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -48
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 0
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 48
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 96
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 144
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 192
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 240
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 288
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 336
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 576
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 624
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 528
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 480
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 432
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 384
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -48
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 0
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 0
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 0
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -48
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -96
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -144
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -192
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -48
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -96
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -144
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -192
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -96
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -144
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -192
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -144
	y: 0
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -144
	y: -96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -96
	y: -96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -144
	y: -192
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: -96
	y: -192
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 576
	y: -192
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 624
	y: -192
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 576
	y: -96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 624
	y: -96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 480
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 480
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 480
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 528
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 576
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 624
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 672
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 672
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 624
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 576
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 528
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 528
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 576
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 624
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 672
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: 336
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: 288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: 240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: 192
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: 0
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: -48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: -96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: -144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: -192
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: -240
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: -288
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 144
	y: 0
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 192
	y: 0
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 240
	y: 0
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 288
	y: 0
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 336
	y: 0
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 336
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 336
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 336
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 288
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 240
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 192
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 144
	y: 144
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 144
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 144
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 192
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 240
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 288
	y: 48
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 288
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 240
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 192
	y: 96
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 0
	y: 0
}
{
	class: com.stat.vod.system.collision.CollisionWall
	x: 720
	y: -336
}
]