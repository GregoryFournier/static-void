package com.stat.vod

import com.stat.vod.screens.base.BaseScreen
import com.stat.vod.screens.base.ScreenManager
import com.stat.vod.screens.dev.map.MapCreationScreen
import com.stat.vod.screens.lobby.LobbyScreen
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.managers.AssetManager
import com.stat.vod.system.managers.MapManager
import ktx.app.KtxGame
import ktx.inject.register

class MainApplication : KtxGame<BaseScreen>(), ScreenManager {

    fun setupManagers() {
        MainContext.install()
        MainContext.context.register {
            bindSingleton<ScreenManager> { this@MainApplication }
        }
    }

    override fun create() {
        super.create()
        setupManagers()
        val assetManager = MainContext.inject<AssetManager>()
        val mapManager = MainContext.inject<MapManager>()
        addScreen(MapCreationScreen())
        addScreen(LobbyScreen(mapManager.loadPlayMap()))
//        setScreen<MapCreationScreen>()
        setScreen<LobbyScreen>()
    }

    override fun goToNewScreen(screen: BaseScreen, disposeCurrent: Boolean) {
        addNewScreen(screen, disposeCurrent)
        returnToScreen(screen::class.java)
    }

    override fun addNewScreen(screen: BaseScreen, disposeCurrent: Boolean) {
        if (disposeCurrent) {
            getCurrentScreen().let {
                removeScreen(it::class.java)?.dispose()
            }
        }
        addScreen(screen)
    }

    override fun <T : BaseScreen> returnToScreen(screenClass: Class<T>) {
        setScreen(screenClass)
    }

    override fun <Type : BaseScreen> addScreen(type: Class<Type>, screen: Type) {
        screens.put(screen.javaClass, screen)
    }

    override fun getCurrentScreen(): BaseScreen {
        return shownScreen
    }
}