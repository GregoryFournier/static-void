package com.stat.vod.screens.play.model

data class TakeOwnershipRequest(val entityUUID: String) {
}