package com.stat.vod.screens.play.model

data class CreateEntityRequest(val entity: String, val uuid: String) {
}