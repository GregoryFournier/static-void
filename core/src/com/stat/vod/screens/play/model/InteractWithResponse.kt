package com.stat.vod.screens.play.model

import com.stat.vod.system.entity.interaction.Interaction

data class InteractWithResponse(
        val interactorUUID: String,
        val otherUUID: String,
        val interaction: Interaction
)