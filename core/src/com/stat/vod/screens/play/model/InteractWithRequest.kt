package com.stat.vod.screens.play.model

import com.stat.vod.screens.lobby.model.PlayerInfo
import com.stat.vod.system.entity.interaction.Interaction

data class InteractWithRequest(
        val interactor: PlayerInfo,
        val otherUUID: String,
        val interaction: Interaction)
