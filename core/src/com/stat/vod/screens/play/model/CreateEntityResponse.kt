package com.stat.vod.screens.play.model

data class CreateEntityResponse(val entity: String, val ownerUUID: String)