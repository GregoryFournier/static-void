package com.stat.vod.screens.play.model

data class TakeOwnershipResponse(val granted: Boolean, val entityUUID: String, val newOwnerUUID: String, val error: String?)