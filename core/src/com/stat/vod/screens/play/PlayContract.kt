package com.stat.vod.screens.play

import com.stat.vod.system.entity.Player
import com.stat.vod.system.entity.base.GameEntity
import com.stat.vod.system.entity.base.PlayerTransform
import com.stat.vod.system.entity.interaction.Interaction

object PlayContract {
    interface Presenter {
        var view: View

        fun interactWith(me: Player, other: GameEntity, interaction: Interaction)
        fun createNetworkEntity(entity: GameEntity, hostCheck: Boolean = true)
    }

    interface View {
        val presenter: Presenter

        fun onInteraction(interactorUUID: String, otherUUID: String, interaction: Interaction)
        fun updatePlayer(transform: PlayerTransform)
        fun createNetworkEntity(entity: GameEntity, ownerUUID: String)
    }
}