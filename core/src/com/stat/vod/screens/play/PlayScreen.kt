package com.stat.vod.screens.play

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.stat.vod.screens.base.BaseScreen
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.engines.life.LifeEngine
import com.stat.vod.system.entity.Player
import com.stat.vod.system.entity.base.GameEntity
import com.stat.vod.system.entity.base.PlayerTransform
import com.stat.vod.system.entity.interaction.Interaction
import com.stat.vod.system.entity.objects.ExampleObject
import com.stat.vod.system.managers.ActionListener
import com.stat.vod.system.managers.GlobalFlagsManager
import com.stat.vod.system.managers.UserInfoManager
import com.stat.vod.system.map.PlayMap
import java.lang.Float

class PlayScreen(private val players: List<Player>, private val map: PlayMap) : BaseScreen("Play Screen"), PlayContract.View {
    val flags: GlobalFlagsManager by lazy { MainContext.inject() }
    val userInfoManager: UserInfoManager by lazy { MainContext.inject() }
    val myPlayer = players.find { it.uuid == userInfoManager.PlayerUUID } ?: error("You're not in the game...?")
    override val presenter: PlayContract.Presenter = PlayPresenter(this)

    override fun show() {
        super.show()
        flags.showCollisionWalls = false
        flags.isInLobby = false
        if (firstShown) {
            crew.addMembers(*map.tiles.toTypedArray())
            crew.addMembers(*map.collisionWalls.toTypedArray())
            crew.addMembers(*players.toTypedArray())
            engines.add(LifeEngine(crew))
            crew.addAction(Actions.forever(Actions.run {
                myPlayer?.let {
                    camera.position.x = it.x
                    camera.position.y = it.y
                }
            }))
            firstShown = false
        }
    }

    override fun render(delta: kotlin.Float) {
        super.render(delta)
    }

    override fun onAction(action: ActionListener.InputAction): Boolean {
        when (action) {
            ActionListener.InputAction.UP -> myPlayer.velocityY = myPlayer.currentMoveSpeed
            ActionListener.InputAction.LEFT -> myPlayer.velocityX = -myPlayer.currentMoveSpeed
            ActionListener.InputAction.DOWN -> myPlayer.velocityY = -myPlayer.currentMoveSpeed
            ActionListener.InputAction.RIGHT -> myPlayer.velocityX = myPlayer.currentMoveSpeed
            ActionListener.InputAction.RUN -> myPlayer.currentMoveSpeed = Player.WALK_SPEED
            ActionListener.InputAction.SPECIAL_1 -> {
                presenter.createNetworkEntity(ExampleObject().apply {
                    setPosition(myPlayer.x + 48, myPlayer.y)
                }, false)
            }
            ActionListener.InputAction.ACTION -> {
                when (myPlayer.currentAction) {
                    Player.PlayerAction.INTERACT -> attemptToInteract(myPlayer, Interaction.DEFAULT)
                    Player.PlayerAction.USE_HELD -> {
                        myPlayer.heldObject?.let { other ->
                            handleInteraction(myPlayer, other, Interaction.DEFAULT)
                        }
                    }
                }
            }
            else -> return false
        }
        return true
    }

    override fun onActionReleased(action: ActionListener.InputAction): Boolean {
        when (action) {
            ActionListener.InputAction.UP -> myPlayer.velocityY = Float.min(0f, myPlayer.velocityY)
            ActionListener.InputAction.LEFT -> myPlayer.velocityX = Float.max(0f, myPlayer.velocityX)
            ActionListener.InputAction.DOWN -> myPlayer.velocityY = Float.max(0f, myPlayer.velocityY)
            ActionListener.InputAction.RIGHT -> myPlayer.velocityX = Float.min(0f, myPlayer.velocityX)
            ActionListener.InputAction.RUN -> myPlayer.currentMoveSpeed = Player.MOVE_SPEED
            else -> return false
        }
        return true
    }

    override fun onInteraction(interactorUUID: String, otherUUID: String, interaction: Interaction) {
        val interactor = crew.findById<GameEntity>(interactorUUID)
        val other = crew.findById<GameEntity>(otherUUID)
        if (interactor != null && other != null) {
            handleInteraction(interactor, other, interaction)
        }
    }

    override fun updatePlayer(transform: PlayerTransform) {
        crew.findById<GameEntity>(transform.uuid)?.setPosition(transform)
    }

    override fun createNetworkEntity(entity: GameEntity, ownerUUID: String) {
        crew.addMember(entity)
    }

    private fun attemptToInteract(interactor: Player, interaction: Interaction) {
        val interactRect = interactor.getInteractRect()
        crew.members.filterIsInstance<GameEntity>().filter {
            it.interactable &&
                    it.uuid != interactor.uuid // We don't want to interact with ourself
        }.find {
            interactRect.contains(it.getPhysicalRect())
        }?.let { other ->
            presenter.interactWith(interactor, other, interaction)
        }
    }

    /**
     * Server authorised, except for held objects
     */
    private fun handleInteraction(interactor: GameEntity, other: GameEntity, interaction: Interaction) {
        val actualInteraction = (if (interaction == Interaction.DEFAULT) {
            other.defaultInteraction
        } else {
            interaction
        }) ?: return

        interactor.onInteractionWith(other, actualInteraction)
        other.onInteraction(interactor, actualInteraction)
    }
}