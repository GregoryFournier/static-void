package com.stat.vod.screens.play

import com.badlogic.gdx.utils.Json
import com.google.gson.Gson
import com.stat.vod.screens.play.model.CreateEntityRequest
import com.stat.vod.screens.play.model.CreateEntityResponse
import com.stat.vod.screens.play.model.InteractWithRequest
import com.stat.vod.screens.play.model.InteractWithResponse
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.entity.Player
import com.stat.vod.system.entity.base.GameEntity
import com.stat.vod.system.entity.base.PlayerTransform
import com.stat.vod.system.entity.interaction.Interaction
import com.stat.vod.system.managers.GlobalFlagsManager
import com.stat.vod.system.managers.UserInfoManager
import com.stat.vod.system.utils.response
import io.socket.client.Socket
import ktx.log.info

class PlayPresenter(override var view: PlayContract.View) : PlayContract.Presenter {
    private val socket: Socket = MainContext.inject()
    private val flags: GlobalFlagsManager = MainContext.inject()
    val userInfoManager: UserInfoManager = MainContext.inject()

    init {
        socket.on(INTERACT_WITH) {
            val response = it.response<InteractWithResponse>()
            view.onInteraction(response.interactorUUID, response.otherUUID, response.interaction)
        }.on(PLAYER_TRANSFORM_UPDATE) { data ->
            val playerTransform = data.response<PlayerTransform>()
            view.updatePlayer(playerTransform)
        }.on(CREATE_ENTITY) { data ->
            val response = data.response<CreateEntityResponse>()
            val entity = Json().fromJson(GameEntity::class.java, response.entity)
//            entity.ownedByMe = response.ownerUUID == userInfoManager.PlayerUUID
            // TODO how to handle ownership. Probably no one owns anything unless they want to move it, at which point they ask the server for ownership.
            info { response.ownerUUID }
            view.createNetworkEntity(entity, response.ownerUUID)
        }
    }

    override fun interactWith(me: Player, other: GameEntity, interaction: Interaction) {
        val request = InteractWithRequest(me.getPlayerInfo(), other.uuid, interaction)
        socket.emit(INTERACT_WITH, Gson().toJson(request))
    }

    override fun createNetworkEntity(entity: GameEntity, hostCheck: Boolean) {
        if (hostCheck && !flags.isHost) return
        val json = Json().prettyPrint(entity)
        val request = CreateEntityRequest(json, entity.uuid)
        socket.emit(CREATE_ENTITY, Gson().toJson(request))
    }

    companion object {
        const val INTERACT_WITH = "play_interact_with"
        const val PLAYER_TRANSFORM_UPDATE = "play_player_transform_update"
        const val CREATE_ENTITY = "play_entity_create"
    }
}