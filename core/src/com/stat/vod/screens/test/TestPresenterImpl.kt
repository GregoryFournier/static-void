package com.stat.vod.screens.test

import com.badlogic.gdx.Gdx
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.entity.base.PlayerTransform
import com.stat.vod.system.utils.response
import io.socket.client.Socket
import ktx.log.info

class TestPresenterImpl(override var view: TestView) : TestPresenter {
    private val socket: Socket = MainContext.inject()

    init {
        socket.on(USER_ENTERED) { response ->
            val playerTransform = response.response<PlayerTransform>()
            info { "User $playerTransform has entered" }
            Gdx.app.postRunnable {
                view.addPlayer(playerTransform)
            }
        }.on(CURRENT_PLAYERS) { data ->
            val players = data.response<Array<PlayerTransform>>()
            Gdx.app.postRunnable {
                players.forEach { transform ->
                    view.addPlayer(transform)
                }
            }
        }.on("player_transform_update") { data ->
            val playerTransform = data.response<PlayerTransform>()
            view.updatePlayer(playerTransform)
        }.on(PLAYER_EXIT) { data ->
            val uuid = data.response<String>()
            view.removePlayer(uuid)
        }
    }

    companion object {
        const val USER_ENTERED = "user_entered"
        const val CURRENT_PLAYERS = "current_players"
        const val PLAYER_EXIT = "user_exit"
    }
}