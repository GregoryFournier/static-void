package com.stat.vod.screens.test

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.stat.vod.screens.base.BaseScreen
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.entity.base.Drawable
import com.stat.vod.system.entity.base.GameEntity
import com.stat.vod.system.entity.base.PlayerTransform
import io.socket.client.Socket
import ktx.app.KtxInputAdapter
import java.lang.Float.max
import java.lang.Float.min

class TestScreen : BaseScreen("Test Screen"), KtxInputAdapter, TestView {
    val member = GameEntity().apply {
        sprite = Drawable.StaticDrawable(Sprite(Texture("badlogic.jpg")))
    }
    override val presenter = TestPresenterImpl(this)

    // socket tests
    val socket: Socket = MainContext.inject()
    val uuid = member.uuid

    init {
        socket.on(Socket.EVENT_CONNECT) {
            println("Connected")
            socket.emit("sign_in", uuid)
        }.on(Socket.EVENT_DISCONNECT) {
            println("Disconnected...")
        }
        socket.connect()

        crew.addMember(member)
    }

    override fun show() {
        super.show()
        Gdx.input.inputProcessor = this
    }

    override fun keyDown(keycode: Int): Boolean {
        when (keycode) {
            Input.Keys.W -> member.velocityY = MOVE_SPEED
            Input.Keys.A -> member.velocityX = -MOVE_SPEED
            Input.Keys.S -> member.velocityY = -MOVE_SPEED
            Input.Keys.D -> member.velocityX = MOVE_SPEED
            else -> return false
        }
        return true
    }

    override fun keyUp(keycode: Int): Boolean {
        when (keycode) {
            Input.Keys.W -> member.velocityY = min(0f, member.velocityY)
            Input.Keys.A -> member.velocityX = max(0f, member.velocityX)
            Input.Keys.S -> member.velocityY = max(0f, member.velocityY)
            Input.Keys.D -> member.velocityX = min(0f, member.velocityX)
            else -> return false
        }
        return true
    }

    override fun addPlayer(transform: PlayerTransform) {
        crew.addMember(GameEntity(transform).apply {
            sprite = Drawable.StaticDrawable(Sprite(Texture("badlogic.jpg")))
        })
    }

    override fun updatePlayer(transform: PlayerTransform) {
        crew.findById<GameEntity>(transform.uuid)?.setPosition(transform.x, transform.y)
    }

    override fun removePlayer(uuid: String) {
        crew.findById<GameEntity>(uuid)?.let {
            crew.removeMember(it)
        }
    }

    companion object {
        const val MOVE_SPEED = 500F
    }
}