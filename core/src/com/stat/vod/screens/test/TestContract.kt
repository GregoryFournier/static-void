package com.stat.vod.screens.test

import com.stat.vod.system.entity.base.PlayerTransform

interface TestPresenter {
    var view: TestView
}

interface TestView {
    val presenter: TestPresenter

    fun addPlayer(transform: PlayerTransform)
    fun updatePlayer(transform: PlayerTransform)
    fun removePlayer(uuid: String)
}