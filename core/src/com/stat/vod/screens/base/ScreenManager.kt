package com.stat.vod.screens.base

interface ScreenManager {
    fun goToNewScreen(screen: BaseScreen, disposeCurrent: Boolean = true)
    fun addNewScreen(screen: BaseScreen, disposeCurrent: Boolean = true)
    fun <T: BaseScreen> returnToScreen(screenClass : Class<T>)
    fun getCurrentScreen(): BaseScreen
}