package com.stat.vod.screens.base

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.engines.base.Engine
import com.stat.vod.system.entity.base.Crew
import com.stat.vod.system.entity.base.GameEntity
import com.stat.vod.system.entity.base.Member
import com.stat.vod.system.managers.ActionListener
import com.stat.vod.system.managers.InputActionManager
import com.stat.vod.system.utils.roundDown
import ktx.app.KtxScreen
import ktx.log.info

abstract class BaseScreen(private val name: String) : KtxScreen, ActionListener {
    private val batch: SpriteBatch = MainContext.inject()
    internal val camera: OrthographicCamera = OrthographicCamera()
    internal val hudCamera: OrthographicCamera = OrthographicCamera()
    internal val viewport: Viewport = FitViewport(WORLD_WIDTH, WORLD_HEIGHT, camera)
    internal val hudViewport: Viewport = FitViewport(WORLD_WIDTH, WORLD_HEIGHT, hudCamera)
    internal val crew: Crew = Crew(batch, camera)
    internal val hudCrew: Crew = Crew(batch, hudCamera)
    internal val engines = ArrayList<Engine<*>>()

    // Background screenspace stuff
    internal val backgroundCrew = Crew(batch, hudCamera)

    internal var firstShown = true

    override fun render(delta: Float) {
        backgroundCrew.act(delta)
        crew.act(delta)
        hudCrew.act(delta)
        engines.forEach { engine ->
            engine.act(delta)
        }
        backgroundCrew.render()
        crew.render()
        hudCrew.render()
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        info { "Resizing $name Screen, with width:$width and height:$height" }
        viewport.update(width, height)
        hudViewport.update(width, height)
    }

    override fun show() {
        super.show()
        MainContext.inject<InputActionManager>().subscribe(this)
    }

    override fun hide() {
        MainContext.inject<InputActionManager>().unsubscribe(this)
        super.hide()
    }

    fun setOwnershipOf(entityUUID: String, ownerUUID: String) {
        crew.findById<GameEntity>(entityUUID)?.let {
            it.owner = ownerUUID
        }
    }

    internal fun getMousePosition(alignmentX: Long = 0L, alignmentY: Long = 0L, cam: Camera = camera): Vector2 {
        val pos = cam.unproject(Vector3(Gdx.input.getX().toFloat(), Gdx.input.getY().toFloat(), 0f))
        if (alignmentX != 0L) {
            pos.x = roundDown(pos.x.toLong(), alignmentX).toFloat()
        }
        if (alignmentY != 0L) {
            pos.y = roundDown(pos.y.toLong(), alignmentY).toFloat()
        }
        return Vector2(pos.x, pos.y)
    }

    internal inline fun <reified T : Member> getMembersUnderMouse(list: ArrayList<Member> = crew.members, cam: Camera = camera): List<T> {
        val mousePos = getMousePosition(cam = cam)
        return list.filterIsInstance<T>().filter {
            it.hit(mousePos.x, mousePos.y, true) != null
        }.sortedByDescending { it.drawIndex }
    }


    companion object {
        const val WORLD_WIDTH = 1920f
        const val WORLD_HEIGHT = 1080f
    }
}