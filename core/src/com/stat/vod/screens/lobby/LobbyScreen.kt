package com.stat.vod.screens.lobby

import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.stat.vod.screens.base.BaseScreen
import com.stat.vod.screens.base.ScreenManager
import com.stat.vod.screens.lobby.model.PlayerInfo
import com.stat.vod.screens.lobby.model.UserInfoResponse
import com.stat.vod.screens.play.PlayScreen
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.entity.Player
import com.stat.vod.system.entity.base.GameEntity
import com.stat.vod.system.entity.base.PlayerTransform
import com.stat.vod.system.managers.ActionListener
import com.stat.vod.system.managers.GlobalFlagsManager
import com.stat.vod.system.managers.UserInfoManager
import com.stat.vod.system.map.PlayMap
import ktx.log.info
import java.lang.Float
import kotlin.math.sign

class LobbyScreen(private val map: PlayMap) : BaseScreen("Lobby Screen"), LobbyContract.View {
    val flags: GlobalFlagsManager = MainContext.inject()
    val userInfoManager: UserInfoManager by lazy { MainContext.inject() }
    private val players: List<Player>
        get() = crew.members.filterIsInstance<Player>()
    var myPlayer: Player? = null

    override val presenter: LobbyContract.Presenter = LobbyPresenter(this)

    override fun show() {
        super.show()
        flags.showCollisionWalls = false
        flags.isInLobby = true
        if (firstShown) {
            presenter.connect()
            crew.addMembers(*map.tiles.toTypedArray())
            crew.addMembers(*map.collisionWalls.toTypedArray())
            crew.addAction(Actions.forever(Actions.run {
                myPlayer?.let {
                    camera.position.x = it.x
                    camera.position.y = it.y
                }
            }))
            firstShown = false
        }
    }

    override fun render(delta: kotlin.Float) {
        super.render(delta)
        checkCollisions()
    }

    override fun onAction(action: ActionListener.InputAction): Boolean {
        myPlayer?.let { player ->
            when (action) {
                ActionListener.InputAction.UP -> player.velocityY = player.currentMoveSpeed
                ActionListener.InputAction.LEFT -> player.velocityX = -player.currentMoveSpeed
                ActionListener.InputAction.DOWN -> player.velocityY = -player.currentMoveSpeed
                ActionListener.InputAction.RIGHT -> player.velocityX = player.currentMoveSpeed
                ActionListener.InputAction.RUN -> player.currentMoveSpeed = Player.WALK_SPEED
                ActionListener.InputAction.ACTION -> {
                    if (flags.isHost) {
                        presenter.startGame()
                    }
                }
                else -> return false
            }
            return true
        } ?: return false

    }

    override fun onActionReleased(action: ActionListener.InputAction): Boolean {
        myPlayer?.let { player ->
            when (action) {
                ActionListener.InputAction.UP -> player.velocityY = Float.min(0f, player.velocityY)
                ActionListener.InputAction.LEFT -> player.velocityX = Float.max(0f, player.velocityX)
                ActionListener.InputAction.DOWN -> player.velocityY = Float.max(0f, player.velocityY)
                ActionListener.InputAction.RIGHT -> player.velocityX = Float.min(0f, player.velocityX)
                ActionListener.InputAction.RUN -> player.currentMoveSpeed = Player.MOVE_SPEED
                else -> return false
            }
            return true
        } ?: return false
    }

    override fun onConnection() {
        info { "I've connected" }
    }

    override fun onUserInfo(response: UserInfoResponse) {
        flags.isHost = response.color == Player.PlayerColor.RED // TODO better way of assigning host
        myPlayer = Player(userInfoManager.PlayerUUID, response.color).apply {
            owner = userInfoManager.PlayerUUID
        }.also {
            crew.addMember(it)
        }
    }

    override fun addPlayer(playerInfo: PlayerInfo) {
        val player = Player(playerInfo).apply {
            setPosition(playerInfo.x, playerInfo.y)
        }
        crew.addMember(player)
    }

    override fun updatePlayer(transform: PlayerTransform) {
        players.find {
            !it.ownedByMe &&
                    it.uuid == transform.uuid
        }?.setPosition(transform)
    }

    override fun removePlayer(uuid: String) {
        players.find { it.uuid == uuid }?.let {
            crew.removeMember(it)
        }
    }

    override fun startGame() {
        MainContext.inject<ScreenManager>().goToNewScreen(PlayScreen(players, map))
    }

    private fun checkCollisions() {
        val entities = crew.members.filterIsInstance<GameEntity>()
        val size = entities.size
        for (i in 0 until size) {
            val entity = entities[i]
            for (j in i + 1 until size) {
                val other = entities[j]
                if (entity.isCollidingWith(other)) {
                    entity.onCollision(other)
                    other.onCollision(entity)
                }
            }
        }
    }

}