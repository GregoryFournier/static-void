package com.stat.vod.screens.lobby

import com.stat.vod.screens.lobby.model.PlayerInfo
import com.stat.vod.screens.lobby.model.UserInfoResponse
import com.stat.vod.system.entity.base.PlayerTransform

object LobbyContract {
    interface Presenter {
        var view: View

        fun connect()
        fun goToPlay()
        fun startGame()
    }

    interface View {
        val presenter: Presenter

        fun onConnection()
        fun onUserInfo(response: UserInfoResponse)
        fun addPlayer(playerInfo: PlayerInfo)
        fun updatePlayer(transform: PlayerTransform)
        fun removePlayer(uuid: String)
        fun startGame()
    }
}
