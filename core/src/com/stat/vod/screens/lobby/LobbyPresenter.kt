package com.stat.vod.screens.lobby

import com.badlogic.gdx.Gdx
import com.stat.vod.screens.lobby.model.PlayerInfo
import com.stat.vod.screens.lobby.model.UserInfoResponse
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.entity.Player
import com.stat.vod.system.entity.base.PlayerTransform
import com.stat.vod.system.managers.UserInfoManager
import com.stat.vod.system.utils.response
import io.socket.client.Socket
import ktx.log.info

class LobbyPresenter(override var view: LobbyContract.View) : LobbyContract.Presenter {
    private val socket: Socket by lazy { MainContext.inject() }
    private val userInfoManager: UserInfoManager by lazy { MainContext.inject() }

    private val screenSpecificEvents =
            arrayOf(ASSIGN_INFO, CURRENT_PLAYERS, PLAYER_TRANSFORM_UPDATE, USER_ENTERED, USER_EXITED, START_GAME)

    override fun connect() {
        socket.on(Socket.EVENT_CONNECT) {
            println("Connected")
            socket.emit("sign_in", userInfoManager.PlayerUUID)
            view.onConnection()
        }.on(Socket.EVENT_DISCONNECT) {
            println("Disconnected...")
        }.on(ASSIGN_INFO) {
            val response = it.response<String>()
            view.onUserInfo(UserInfoResponse(Player.PlayerColor.valueOf(response)))
        }.on(CURRENT_PLAYERS) { data ->
            val players = data.response<Array<PlayerInfo>>()
            Gdx.app.postRunnable {
                players.forEach { transform ->
                    view.addPlayer(transform)
                }
            }
        }.on(USER_ENTERED) { response ->
            val playerTransform = response.response<PlayerInfo>()
            info { "User $playerTransform has entered" }
            Gdx.app.postRunnable {
                view.addPlayer(playerTransform)
            }
        }.on(PLAYER_TRANSFORM_UPDATE) { data ->
            val playerTransform = data.response<PlayerTransform>()
            view.updatePlayer(playerTransform)
        }.on(USER_EXITED) { data ->
            val uuid = data.response<String>()
            view.removePlayer(uuid)
        }.on(START_GAME) {
            Gdx.app.postRunnable {
                goToPlay()
            }
        }
        socket.connect()
    }

    override fun goToPlay() {
        screenSpecificEvents.forEach {
            socket.off(it)
        }
        view.startGame()
    }

    override fun startGame() {
        socket.emit(START_GAME)
    }

    companion object {
        const val ASSIGN_INFO = "assign_info"
        const val CURRENT_PLAYERS = "current_players"
        const val PLAYER_TRANSFORM_UPDATE = "lobby_player_transform_update"
        const val USER_ENTERED = "lobby_user_entered"
        const val USER_EXITED = "lobby_user_exit"
        private const val START_GAME = "lobby_start_game"
    }
}