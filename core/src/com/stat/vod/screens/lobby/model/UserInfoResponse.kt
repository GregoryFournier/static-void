package com.stat.vod.screens.lobby.model

import com.stat.vod.system.entity.Player

data class UserInfoResponse(val color: Player.PlayerColor) {
}