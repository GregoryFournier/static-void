package com.stat.vod.screens.lobby.model

import com.stat.vod.system.entity.Player

/**
 * Used by network to transfer general knowledge on a particular player
 */
data class PlayerInfo(
        val uuid: String,
        val x: Int,
        val y: Int,
        val color: Player.PlayerColor,
        val currentHealth: Int
)