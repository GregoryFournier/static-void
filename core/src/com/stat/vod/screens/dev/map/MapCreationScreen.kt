package com.stat.vod.screens.dev.map

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.NinePatch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Action
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.utils.Json
import com.stat.vod.screens.dev.DevScreen
import com.stat.vod.system.collision.CollisionWall
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.entity.base.Drawable
import com.stat.vod.system.entity.base.Member
import com.stat.vod.system.managers.ActionListener
import com.stat.vod.system.managers.Asset
import com.stat.vod.system.managers.AssetManager.Companion.get
import com.stat.vod.system.managers.GlobalFlagsManager
import com.stat.vod.system.statemachine.StateMachine
import com.stat.vod.system.tiles.AllTilesets
import com.stat.vod.system.tiles.Tile
import com.stat.vod.system.tiles.TileSetMember
import ktx.log.info
import java.lang.Float.max
import java.lang.Float.min

class MapCreationScreen : DevScreen("Map Creation"), StateMachine.StateHolder<PlacementStateMachine.State, PlacementStateMachine.Action> {
    val allTileSets = MainContext.inject<AllTilesets>()
    val tileSets = allTileSets.getAll()
    var currentTileMember = TileSetMember(tileSets[1]).apply {
        setPosition(-WORLD_WIDTH / 2, -48 * 10f)
    }
    val cursor = Member(Drawable.ScalableDrawable(NinePatch(cursorAsset.get(), 1, 1, 1, 1)))
    var cursorPlacementAction: Action? = null
    var cursorSizeAction: Action? = null

    val flags = MainContext.inject<GlobalFlagsManager>()

    // Placement controls
    var placementState = PlacementStateMachine(this)
    var currentTile: Tile? = null
    var isPlacing = false
    var placedPositions = ArrayList<Vector2>()
    var editing = true

    // Camera controls
    val cameraSpeed = 60f
    var cameraVelocity = Vector2(0f, 0f)

    init {
        cursor.sprite.width = 48f
        cursor.sprite.height = 48f
        flags.showCollisionWalls = true
        hudCrew.addMembers(currentTileMember, cursor)
        cursorPlacementAction = cursorPositionAction().apply {
            cursor.addAction(this)
        }
        crew.addAction(Actions.forever(Actions.run {
            camera.position.x += cameraVelocity.x
            camera.position.y += cameraVelocity.y
        }))
        crew.addAction(placementAction())
        crew.addAction(removeTileAction())
    }

    override fun onAction(action: ActionListener.InputAction): Boolean {
        super.onAction(action)
        when (action) {
            ActionListener.InputAction.CLICK -> {
                when (placementState.state) {
                    is PlacementStateMachine.State.TilePlacement -> {
                        if (currentTile == null && !isPlacing) {
                            cursorPlacementAction?.let {
                                cursor.removeAction(it)
                            }
                            cursor.addAction(cursorSizeAction())
                            placedPositions.clear()
                        } else {
                            isPlacing = true
                        }
                    }
                    is PlacementStateMachine.State.WallPlacement -> {
                        isPlacing = true
                    }
                }
            }
            ActionListener.InputAction.RIGHT_CLICK -> {
                val state = placementState.state
                when (state) {
                    is PlacementStateMachine.State.TilePlacement -> {
                        if (currentTile != null) {
                            currentTile?.removeFromCrew()
                            currentTile = null
                        } else {
                            state.isDeleting = true
                        }
                    }
                    is PlacementStateMachine.State.WallPlacement -> {
                        state.isDeleting = true
                    }
                }
            }
            ActionListener.InputAction.UP -> cameraVelocity.y = cameraSpeed
            ActionListener.InputAction.DOWN -> cameraVelocity.y = -cameraSpeed
            ActionListener.InputAction.LEFT -> cameraVelocity.x = -cameraSpeed
            ActionListener.InputAction.RIGHT -> cameraVelocity.x = cameraSpeed
            else -> return false
        }
        return true
    }

    override fun keyDown(keycode: Int): Boolean {
        when (keycode) {
            Input.Keys.NUM_1 -> {
                currentTileMember.tileSet = tileSets[0]
            }
            Input.Keys.NUM_2 -> {
                currentTileMember.tileSet = tileSets[1]
            }
            Input.Keys.TAB -> {
                placementState.transition(PlacementStateMachine.Action.NextState)
                flags.showCollisionWalls = placementState.state is PlacementStateMachine.State.WallPlacement
            }
            Input.Keys.S -> {
                if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {
                    info { "We are saving level" }
                    saveTempSetup()
                }
            }
            Input.Keys.O -> {
                if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {
                    loadLevel()
                }
            }
            else -> return false
        }
        return true
    }

    override fun scrolled(amount: Int): Boolean {
        val currentIndex = tileSets.indexOf(currentTileMember.tileSet)
        if (amount > 0) {
            currentTileMember.tileSet = tileSets[(currentIndex + 1) % tileSets.size]
        } else {
            currentTileMember.tileSet = if (currentIndex == 0) {
                tileSets.last()
            } else {
                tileSets[currentIndex - 1]
            }
        }
        return true
    }

    override fun onActionReleased(action: ActionListener.InputAction): Boolean {
        when (action) {
            ActionListener.InputAction.CLICK -> {
                cursorSizeAction?.let {
                    grabNewTile()
                    cursor.sprite.width = 48f
                    cursor.sprite.height = 48f
                    cursor.removeAction(it)
                    cursor.addAction(cursorPositionAction())
                    cursorSizeAction = null
                }
                isPlacing = false
            }
            ActionListener.InputAction.RIGHT_CLICK -> {
                val state = placementState.state
                when (state) {
                    is PlacementStateMachine.State.TilePlacement -> {
                        state.isDeleting = false
                    }
                    is PlacementStateMachine.State.WallPlacement -> {
                        state.isDeleting = false
                    }
                }
            }
            ActionListener.InputAction.UP -> cameraVelocity.y = min(0f, cameraVelocity.y)
            ActionListener.InputAction.DOWN -> cameraVelocity.y = max(0f, cameraVelocity.y)
            ActionListener.InputAction.LEFT -> cameraVelocity.x = max(0f, cameraVelocity.x)
            ActionListener.InputAction.RIGHT -> cameraVelocity.x = min(0f, cameraVelocity.x)
            else -> return false
        }
        return true
    }

    // States

    override fun onStateEnter(newState: PlacementStateMachine.State) {
        super.onStateEnter(newState)
        when (newState) {
            is PlacementStateMachine.State.TilePlacement -> {
                // Do nothing
            }
            is PlacementStateMachine.State.WallPlacement -> {
                hudCrew.addMember(newState.wall)
                newState.wall.addAction(Actions.forever(Actions.run {
                    val newPosition = getMousePosition(newState.wall.width.toLong(), newState.wall.height.toLong(), cam = hudCamera)
                    newState.wall.setPosition(newPosition)
                }))
                currentTile?.removeFromCrew()
                currentTile = null
                placedPositions.clear()
            }
        }
    }

    override fun onStateExit(oldState: PlacementStateMachine.State) {
        super.onStateExit(oldState)
        when (oldState) {
            is PlacementStateMachine.State.TilePlacement -> {
                currentTile?.removeFromCrew()
                currentTile = null
                placedPositions.clear()
                isPlacing = false
            }
            is PlacementStateMachine.State.WallPlacement -> {
                oldState.wall.removeFromCrew()
            }
        }
    }

    private fun grabNewTile() {
        getMembersUnderMouse<TileSetMember>(hudCrew.members, hudCamera).firstOrNull()?.let { tileMember ->
            val mousePos = cursor.getPosition()
            mousePos.y += cursor.sprite.height - 48
            val delta = Vector2(mousePos.x - tileMember.x,
                    tileMember.y - mousePos.y + tileMember.height - tileMember.tileSet.tileHeight)
            currentTile?.removeFromCrew()
            currentTile = Tile(tileMember.tileSet, delta.x.toInt(), delta.y.toInt(), cursor.sprite.width.toInt(), cursor.sprite.height.toInt()).also { tile ->
                hudCrew.addMember(tile)
                tile.addAction(Actions.forever(Actions.run {
                    val newPosition = getMousePosition(48L, 48L, cam = hudCamera)
                    tile.setPosition(newPosition)
                }))
            }
        }
    }

    private fun placeTile(tile: Tile, newPosition: Vector2) {
        val newTile = tile.copy().apply {
            setPosition(newPosition)
        }
        newTile.actions.clear()
        crew.addMember(newTile)
    }

    private fun placeWall(newPosition: Vector2) {
        val newWall = CollisionWall().apply {
            setPosition(newPosition)
        }
        newWall.actions.clear()
        crew.addMember(newWall)
    }

    private fun placementAction(): Action {
        return Actions.forever(Actions.run {
            if (!isPlacing) return@run
            val state = placementState.state
            when (state) {
                is PlacementStateMachine.State.TilePlacement -> {
                    currentTile?.let { tile ->
                        val newPosition = getMousePosition(tile.tileset.tileWidth.toLong(), tile.tileset.tileHeight.toLong())
                        if (placedPositions.contains(newPosition)) return@run
                        placeTile(tile, newPosition)
                        println("Place a new tile")
                        placedPositions.add(newPosition)
                    }
                }
                is PlacementStateMachine.State.WallPlacement -> {
                    val wall = state.wall
                    val newPosition = getMousePosition(wall.width.toLong(), wall.width.toLong())
                    if (placedPositions.contains(newPosition)) return@run
                    placeWall(newPosition)
                    println("Place a new wall")
                    placedPositions.add(newPosition)
                }
            }

        })
    }

    private fun removeTileAction(): Action {
        return Actions.forever(Actions.run {
            val state = placementState.state
            when (state) {
                is PlacementStateMachine.State.TilePlacement -> {
                    if (!state.isDeleting) return@run
                    removeTileAtCursor()
                }
                is PlacementStateMachine.State.WallPlacement -> {
                    if (!state.isDeleting) return@run
                    removeWallAtCursor()
                }
            }
        })
    }

    private fun cursorPositionAction(): Action {
        cursorPlacementAction?.let {
            cursor.removeAction(it)
        }
        return Actions.forever(Actions.run {
            cursor.shouldDraw = currentTile == null
            val mousePos = getMousePosition(48, 48, hudCamera)
            cursor.setPosition(mousePos)
        })
    }

    private fun cursorSizeAction(): Action {
        cursorSizeAction?.let {
            cursor.removeAction(it)
        }
        val initialMousePos = getMousePosition(48L, 48L, hudCamera)
        cursorSizeAction = Actions.forever(Actions.run {
            val delta = getMousePosition(48L, 48L, hudCamera).add(48f, 48f).sub(initialMousePos)
            cursor.sprite.width = delta.x.coerceAtLeast(48f)
            cursor.sprite.height = delta.y.coerceAtLeast(48f)
        })
        return cursorSizeAction!!
    }

    private fun removeTileAtCursor() {
        getMembersUnderMouse<Tile>().firstOrNull()?.removeFromCrew()
    }

    private fun removeWallAtCursor() {
        getMembersUnderMouse<CollisionWall>().firstOrNull()?.removeFromCrew()
    }

    // File handling

    private fun saveTempSetup() {
        val file = Gdx.files.local(TEMP_LEVEL_PATH)
        file.writeString(Json().prettyPrint(crew.members), false)
    }

    private fun loadLevel() {
        crew.removeAllMembers()
        val file = Gdx.files.local(TEMP_LEVEL_PATH)
        val type = ArrayList<Member>()
        val list: ArrayList<Member> = Json().fromJson(type::class.java, file.readString())
        list.forEach {
            crew.addMember(it)
        }
    }

    companion object {
        const val TEMP_LEVEL_PATH = "data/all_setups_backup.sv"

        @Asset
        val cursorAsset = AssetDescriptor("dev/cursor.png", Texture::class.java)
    }

}