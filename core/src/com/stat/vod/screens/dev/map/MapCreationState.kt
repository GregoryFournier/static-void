package com.stat.vod.screens.dev.map

import com.stat.vod.system.collision.CollisionWall
import com.stat.vod.system.statemachine.StateMachine
import com.stat.vod.system.tiles.Tile
import kotlin.reflect.KClass

class PlacementStateMachine(stateholder: StateHolder<PlacementStateMachine.State, PlacementStateMachine.Action>)
    : StateMachine<PlacementStateMachine.State, PlacementStateMachine.Action>(stateholder) {
    override var state: State = State.TilePlacement()

    sealed class Action {
        object NextState: Action()
    }

    sealed class State : StateMachine.State<Action>() {
        data class TilePlacement(var currentTile: Tile? = null) : State() {
            override val transitions: HashMap<KClass<*>, (Action) -> StateMachine.State<Action>>
                    = hashMapOf(Action.NextState::class to {
                WallPlacement(CollisionWall())
            })

            var isDeleting = false
        }

        data class WallPlacement(val wall: CollisionWall) : State() {
            override val transitions: HashMap<KClass<*>, (Action) -> StateMachine.State<Action>> =
                    hashMapOf(Action.NextState::class to {
                        TilePlacement(null)
                    })
            var isDeleting = false
        }
    }
}

