package com.stat.vod.screens.dev

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.stat.vod.screens.base.BaseScreen
import ktx.app.KtxInputAdapter

abstract class DevScreen(name: String): BaseScreen(name), KtxInputAdapter {

    override fun show() {
        super.show()
        val inputProcessor = Gdx.input.inputProcessor
        val multiplexer = InputMultiplexer(this, inputProcessor)
        Gdx.input.inputProcessor = multiplexer
    }

    override fun hide() {
        super.hide()
        (Gdx.input.inputProcessor as? InputMultiplexer)?.removeProcessor(this)
    }
}