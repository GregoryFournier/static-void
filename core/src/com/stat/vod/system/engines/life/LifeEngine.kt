package com.stat.vod.system.engines.life

import com.stat.vod.system.context.MainContext
import com.stat.vod.system.engines.base.Engine
import com.stat.vod.system.entity.base.Crew
import com.stat.vod.system.entity.base.GameEntity
import com.stat.vod.system.utils.response
import io.socket.client.Socket

/**

 * Checks whether or not entities are alive
 */
class LifeEngine(crew: Crew) : Engine<GameEntity>(crew) {
    private val socket: Socket by lazy { MainContext.inject() }
    override val entities: List<GameEntity>
        get() = crew.filterIsInstance()

    init {
        socket.on(HEALTH_CHANGE) {
            val response = it.response<HealthChangeResponse>()
            crew.findById<GameEntity>(response.entityUUID)?.changeHealth(response.healthDelta, false)
        }.on(ENTITY_DEAD) {
            val uuid = it.response<String>()
            crew.findById<GameEntity>(uuid)?.let { entity ->
                handleDeath(entity)
            }
        }
    }

    override fun act(delta: Float) {
        entities.forEach { entity ->
            if (entity.currentHealth <= 0) {
                socket.emit(ENTITY_DEAD, entity.uuid)
                handleDeath(entity)
            }
        }
    }

    private fun handleDeath(entity: GameEntity) {
        entity.onDeath()
        crew.removeMember(entity)
    }

    companion object {
        private const val ENTITY_DEAD = "play_entity_dead"
        const val HEALTH_CHANGE = "play_health_change"
    }
}