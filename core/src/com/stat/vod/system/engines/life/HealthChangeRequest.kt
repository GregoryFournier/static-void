package com.stat.vod.system.engines.life

data class HealthChangeRequest(val entityUUID: String, val healthDelta: Int)