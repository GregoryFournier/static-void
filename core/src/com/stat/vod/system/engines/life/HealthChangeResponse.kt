package com.stat.vod.system.engines.life

class HealthChangeResponse(val entityUUID: String, val healthDelta: Int)