package com.stat.vod.system.engines.base

import com.stat.vod.system.entity.base.Crew
import com.stat.vod.system.entity.base.Member

abstract class Engine<out T : Member>(val crew: Crew) {
    abstract val entities: List<T>

    abstract fun act(delta: Float)
}