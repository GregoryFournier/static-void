package com.stat.vod.system.map

import com.stat.vod.system.collision.CollisionWall
import com.stat.vod.system.tiles.Tile

data class PlayMap(val tiles: List<Tile>, val collisionWalls: List<CollisionWall>) {
}