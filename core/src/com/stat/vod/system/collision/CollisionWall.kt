package com.stat.vod.system.collision

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonValue
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.entity.base.GameEntity
import com.stat.vod.system.managers.Asset
import com.stat.vod.system.managers.AssetManager.Companion.get
import com.stat.vod.system.managers.GlobalFlagsManager
import ktx.json.readValue

class CollisionWall: GameEntity(wallAsset.get()), Json.Serializable {
    override var shouldDraw = false
    get() = flags.showCollisionWalls

    init {
        name = "Wall"
        interactable = false
    }

    override fun write(json: Json) {
        json.writeValue("x", x)
        json.writeValue("y", y)
    }

    override fun read(json: Json, jsonData: JsonValue) {
        x = json.readValue(jsonData, "x")
        y = json.readValue(jsonData, "y")
    }

    companion object {
        @Asset
        val wallAsset = AssetDescriptor("dev/wall.png", Texture::class.java)
    }
}