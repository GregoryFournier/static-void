package com.stat.vod.system.context

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.profiling.GLProfiler
import com.stat.vod.system.managers.*
import com.stat.vod.system.tiles.AllTilesets
import com.stat.vod.system.tiles.Tileset
import io.socket.client.IO
import ktx.inject.Context
import ktx.inject.register
import org.reflections.Reflections
import org.reflections.scanners.FieldAnnotationsScanner
import org.reflections.scanners.SubTypesScanner
import org.reflections.scanners.TypeAnnotationsScanner

object MainContext {
    val context = Context()

    fun install() {
        context.register {
            bindSingleton { Reflections("com.stat", FieldAnnotationsScanner(), TypeAnnotationsScanner(), SubTypesScanner()) }
            bindSingleton {
                AssetManager().apply {
                    while (!update()) {
                        // do nothing
                    }
                }
            }
            bindSingleton { SpriteBatch() }
            bindSingleton { IO.socket("http://localhost:3000") }
            bindSingleton { InputActionManager() }
            bindSingleton { GlobalFlagsManager() }
            bindSingleton { AllTilesets() }
            bind { inject<AllTilesets>().get(Tileset.SetName.WOOD1) }
            bindSingleton { UserInfoManager(inject()) }
            bind { MapManager() }
            bind { OwnershipNetworkManager(inject(),inject()) }
        }
    }

    inline fun <reified T : Any> inject(): T {
        return context.inject()
    }
}