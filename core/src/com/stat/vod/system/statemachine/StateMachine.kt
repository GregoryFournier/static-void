package com.stat.vod.system.statemachine

import ktx.log.info
import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
abstract class StateMachine<S: StateMachine.State<A>,A : Any>(val stateholder: StateHolder<S,A>) {
    abstract var state: S

    fun transition(action: A) {
        (state.transitions[action::class]?.invoke(action) as? S)?.let { newState ->
            stateholder.onStateExit(state)
            state = newState
            stateholder.onStateEnter(newState)
        }
    }

    interface StateHolder<S: State<A>,A : Any> {
        open fun onStateEnter(newState: S) {
            info { "Enter ${newState::class.simpleName} state" }
        }

        open fun onStateExit(oldState: S) {
            info { "Exit ${oldState::class.simpleName} state" }
        }
    }

    abstract class State<A> {
        abstract val transitions: HashMap<KClass<*>,((A) -> State<A>)>
        open fun enter() {}
        open fun exit() {}
    }
}