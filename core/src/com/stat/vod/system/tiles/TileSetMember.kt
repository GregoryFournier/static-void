package com.stat.vod.system.tiles

import com.stat.vod.system.entity.base.Drawable
import com.stat.vod.system.entity.base.Member

class TileSetMember(tilesetTemp: Tileset): Member(Drawable.StaticDrawable(tilesetTemp.texture))  {

    var tileSet = tilesetTemp
    set(value) {
        field = value
        sprite = Drawable.StaticDrawable(value.texture)
    }

}