package com.stat.vod.system.tiles

import com.badlogic.gdx.graphics.Texture

data class Tileset(
        val setName: SetName,
        val texture: Texture,
        val uid: Int,
        val tileWidth: Int = DEFAULT_TILE_WIDTH,
        val tileHeight: Int = DEFAULT_TILE_HEIGHT) {

    enum class SetName {
        WOOD1,
        WOOD2,
        KITCHEN1,
        ITEMS1,
        ITEMS2
    }

    companion object {
        const val DEFAULT_TILE_WIDTH = 48
        const val DEFAULT_TILE_HEIGHT = 48
    }
}