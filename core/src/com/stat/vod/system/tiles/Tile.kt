package com.stat.vod.system.tiles

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonValue
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.entity.base.Drawable
import com.stat.vod.system.entity.base.Member
import com.stat.vod.system.utils.readValue
import ktx.json.readValue

data class Tile(var tileset: Tileset = MainContext.inject(),
                var regionX: Int = 0,
                var regionY: Int = 0,
                var tileWidth: Int = tileset.tileWidth,
                var tileHeight: Int = tileset.tileHeight) : Member(), Json.Serializable {
    init {
        updateSpriteForTileset()
    }

    override fun write(json: Json) {
        json.writeValue(Tile::tileset.name, tileset.setName)
        json.writeValue(Tile::regionX.name, regionX)
        json.writeValue(Tile::regionY.name, regionY)
        json.writeValue(Tile::tileWidth.name, tileWidth)
        json.writeValue(Tile::tileHeight.name, tileHeight)
        json.writeValue("x", x)
        json.writeValue("y", y)
    }

    override fun read(json: Json, jsonData: JsonValue) {
        tileset = MainContext.inject<AllTilesets>().get(json.readValue(jsonData, Tile::tileset.name))
        regionX = json.readValue(jsonData, Tile::regionX.name, 0)
        regionY = json.readValue(jsonData, Tile::regionY.name, 0)
        tileWidth = json.readValue(jsonData, Tile::tileWidth.name, 0)
        tileHeight = json.readValue(jsonData, Tile::tileHeight.name, 0)
        x = json.readValue(jsonData, "x")
        y = json.readValue(jsonData, "y")
        updateSpriteForTileset()
        setPosition(x, y)
    }

    private fun updateSpriteForTileset() {
        sprite = Drawable.RegionDrawable(TextureRegion(tileset.texture, regionX, regionY, tileWidth, tileHeight))
    }
}