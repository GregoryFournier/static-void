package com.stat.vod.system.tiles

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Texture
import com.stat.vod.system.managers.Asset
import com.stat.vod.system.managers.AssetManager.Companion.get

class AllTilesets {

    private val tilesets = hashMapOf(
            Tileset.SetName.WOOD1 to Tileset(Tileset.SetName.WOOD1, wood1.get(), 1),
            Tileset.SetName.WOOD2 to Tileset(Tileset.SetName.WOOD2, wood2.get(), -1),
            Tileset.SetName.KITCHEN1 to Tileset(Tileset.SetName.KITCHEN1, kitchen1.get(), -1),
            Tileset.SetName.ITEMS1 to Tileset(Tileset.SetName.ITEMS1, items1.get(), 6),
            Tileset.SetName.ITEMS2 to Tileset(Tileset.SetName.ITEMS2, items2.get(), 11)
    )

    fun get(name: Tileset.SetName): Tileset {
        return tilesets[name]!!
    }

    fun getById(uid: Int): Tileset {
        return tilesets.values.first { it.uid == uid }
    }

    fun getAll(): Array<Tileset> {
        return tilesets.values.toTypedArray()
    }

    companion object {
        @Asset
        val wood1 = AssetDescriptor("tilesets/wood_1.png", Texture::class.java)

        @Asset
        val wood2 = AssetDescriptor("tilesets/wood_2.png", Texture::class.java)

        @Asset
        val kitchen1 = AssetDescriptor("tilesets/kitchen_1.png", Texture::class.java)

        @Asset
        val items1 = AssetDescriptor("tilesets/items_1.png", Texture::class.java)

        @Asset
        val items2 = AssetDescriptor("tilesets/items_2.png", Texture::class.java)
    }
}