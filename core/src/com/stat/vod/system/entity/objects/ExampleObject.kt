package com.stat.vod.system.entity.objects

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.entity.base.GameEntity
import com.stat.vod.system.entity.interaction.Interaction
import com.stat.vod.system.managers.Asset
import com.stat.vod.system.managers.AssetManager.Companion.get
import com.stat.vod.system.managers.OwnershipNetworkManager
import ktx.log.info

class ExampleObject() : GameEntity(ballAsset.get()) {

    init {
        possibleInteractions.addAll(arrayOf(Interaction.GRAB, Interaction.THROW))
    }

    override fun onInteraction(other: GameEntity, interaction: Interaction) {
        info { interaction.toString() }
//        val manager = MainContext.inject<OwnershipNetworkManager>()
//        if (!ownedByMe) {
//            manager.requestOwnershipOf(uuid) {
//                addAction(Actions.forever(Actions.run {
//                    setPosition(other.getPosition())
//                }))
//            }
//        } else {
//            manager.removeOwnershipOf(uuid) {
//                actions.clear()
//            }
//        }
    }

    companion object {
        @Asset
        val ballAsset = AssetDescriptor("entities/ball.png", Texture::class.java)
    }
}