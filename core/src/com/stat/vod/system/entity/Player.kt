package com.stat.vod.system.entity

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.stat.vod.screens.lobby.model.PlayerInfo
import com.stat.vod.screens.test.TestScreen
import com.stat.vod.system.collision.CollisionWall
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.entity.base.Drawable
import com.stat.vod.system.entity.base.GameEntity
import com.stat.vod.system.entity.interaction.Interaction
import com.stat.vod.system.managers.Asset
import com.stat.vod.system.managers.AssetManager.Companion.get
import com.stat.vod.system.managers.OwnershipNetworkManager

class Player(uuid: String, val color: PlayerColor) : GameEntity(uuid) {
    var orientation: Player.PlayerOrientation = PlayerOrientation.UP
    var heldObject: GameEntity? = null
    val currentAction: PlayerAction
        get() {
            return if (heldObject == null) {
                PlayerAction.INTERACT
            } else {
                PlayerAction.USE_HELD
            }
        }

    init {
        name = "Player ${color.name}"
        val asset = color.toAsset()
        sprite = Drawable.RegionDrawable(TextureRegion(asset, 48, 0, PLAYER_VISUAL_WIDTH, PLAYER_VISUAL_HEIGHT))
        maxHealth = MAX_HEALTH
        currentHealth = maxHealth
        possibleInteractions.add(Interaction.KILL)
    }

    constructor(info: PlayerInfo) : this(info.uuid, info.color) {
        setPosition(info.x, info.y)
    }

    override fun act(delta: Float) {
        super.act(delta)
        updateSpriteOrientation()
        updateHeldObject()
    }

    override fun onCollision(other: GameEntity) {
        super.onCollision(other)
        when (other) {
            is CollisionWall -> {
                val delta = Gdx.graphics.deltaTime
                val normalizedVelocity = Vector2(-velocityX, -velocityY).nor()
                moveBy((delta * normalizedVelocity.x * TestScreen.MOVE_SPEED), (delta * normalizedVelocity.y * TestScreen.MOVE_SPEED))
                velocityX = 0f
                velocityY = 0f
            }
        }
    }

    /**
     * interaction here has already been transformed
     */
    override fun onInteractionWith(other: GameEntity, interaction: Interaction) {
        val ownershipManager: OwnershipNetworkManager = MainContext.inject()
        when (interaction) {
            Interaction.GRAB -> {
                ownershipManager.requestOwnershipOf(other.uuid) {
                    heldObject = other
                    other.possibleInteractions.remove(Interaction.GRAB)
                }
            }
            Interaction.THROW -> {
                if (other == heldObject) heldObject = null
                val dir = orientation.dir
                other.addAction(Actions.sequence(
                        Actions.moveBy(dir.x * 48 * 3f, dir.y * 48 * 3f, 1f, Interpolation.circleOut),
                        Actions.run {
                            ownershipManager.removeOwnershipOf(other.uuid)
                        }))
                other.possibleInteractions.add(0, Interaction.GRAB)
            }
            Interaction.KILL -> {
                other.currentHealth = 0
            }
        }
    }

    fun getPlayerInfo(): PlayerInfo {
        return PlayerInfo(
                uuid,
                x.toInt(),
                y.toInt(),
                color,
                currentHealth
        )
    }

    private fun updateSpriteOrientation() {
        val regionSprite = (sprite as Drawable.RegionDrawable).region
        when {
            velocityX > 0 -> {
                orientation = PlayerOrientation.RIGHT
                regionSprite.setRegion(0, 0, PLAYER_VISUAL_WIDTH, PLAYER_VISUAL_HEIGHT)
            }
            velocityX < 0 -> {
                orientation = PlayerOrientation.LEFT
                regionSprite.setRegion(PLAYER_VISUAL_WIDTH * 2, 0, PLAYER_VISUAL_WIDTH, PLAYER_VISUAL_HEIGHT)
            }
            velocityY > 0 -> {
                orientation = PlayerOrientation.UP
                regionSprite.setRegion(PLAYER_VISUAL_WIDTH, 0, PLAYER_VISUAL_WIDTH, PLAYER_VISUAL_HEIGHT)
            }
            velocityY < 0 -> {
                orientation = PlayerOrientation.DOWN
                regionSprite.setRegion(PLAYER_VISUAL_WIDTH * 3, 0, PLAYER_VISUAL_WIDTH, PLAYER_VISUAL_HEIGHT)
            }
        }
    }

    private fun updateHeldObject() {
        heldObject?.setPosition(getPosition())
    }

    enum class PlayerColor {
        RED, BLUE, PURPLE, REDDER, PINK, BLACK;

        fun toAsset(): Texture {
            return when (this) {
                RED -> redIdleAsset.get()
                BLUE -> blueIdleAsset.get()
                PURPLE -> purpleIdleAsset.get()
                REDDER -> redderIdleAsset.get()
                PINK -> pinkIdleAsset.get()
                BLACK -> blackIdleAsset.get()
            }
        }
    }

    enum class PlayerOrientation(val dir: Vector2) {
        UP(Vector2(0f,1f)),
        DOWN(Vector2(0f,-1f)),
        LEFT(Vector2(-1f,0f)),
        RIGHT(Vector2(1f,0f))
    }

    enum class PlayerAction {
        INTERACT, USE_HELD
    }

    companion object {
        const val PLAYER_VISUAL_WIDTH = 48
        const val PLAYER_VISUAL_HEIGHT = 96
        const val MOVE_SPEED = 175F
        const val WALK_SPEED = 125F
        const val MAX_HEALTH = 2

        @Asset
        val redIdleAsset = AssetDescriptor("players/1/idle.png", Texture::class.java)

        @Asset
        val blueIdleAsset = AssetDescriptor("players/2/idle.png", Texture::class.java)

        @Asset
        val purpleIdleAsset = AssetDescriptor("players/3/idle.png", Texture::class.java)

        @Asset
        val redderIdleAsset = AssetDescriptor("players/4/idle.png", Texture::class.java)

        @Asset
        val pinkIdleAsset = AssetDescriptor("players/5/idle.png", Texture::class.java)

        @Asset
        val blackIdleAsset = AssetDescriptor("players/6/idle.png", Texture::class.java)
    }
}