package com.stat.vod.system.entity.interaction

enum class Interaction {
    DEFAULT, GRAB, THROW, KILL
}