package com.stat.vod.system.entity.base

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Action
import com.badlogic.gdx.scenes.scene2d.Actor

open class Member(var sprite: Drawable = Drawable.None) : Actor() {
    var crew: Crew? = null
    var drawIndex = 0
    var shouldAct = true
    open var shouldDraw = true
    private val actions: ArrayList<Action> = ArrayList()

    constructor(texture: Texture, x: Int = 0, y: Int = 0) : this() {
        val tempSprite = Sprite(texture).apply {
            setPosition(x.toFloat(), y.toFloat())
        }
        this.sprite = Drawable.StaticDrawable(tempSprite)
    }

    override fun act(delta: Float) {
        super.act(delta)
        width = width
        height = height
        sprite.updateTransform(x, y, rotation)
    }

    open fun onAddedToCrew(crew: Crew) {}
    open fun onRemovedFromCrew(crew: Crew) {}

    open fun draw(batch: SpriteBatch, parentAlpha: Float) {
        sprite.draw(batch)
    }

    fun removeFromCrew() {
        crew?.removeMember(this)
    }

    // Transform

    override fun getWidth(): Float {
        return sprite.width
    }

    override fun getHeight(): Float {
        return sprite.height
    }

    override fun hit(x: Float, y: Float, touchable: Boolean): Actor? {
        val rectangle = Rectangle(this.x, this.y, this.width, this.height)
        return if (rectangle.contains(x,y)) {
            this
        } else {
            null
        }
    }

    // Basic Movements

    fun setPosition(x: Int, y: Int) {
        setPosition(x.toFloat(), y.toFloat())
    }

    fun setPosition(transform: PlayerTransform) {
        setPosition(transform.x, transform.y)
    }

    fun setPosition(vector: Vector2) {
        setPosition(vector.x, vector.y)
    }

    fun getPosition(): Vector2 {
        return Vector2(x, y)
    }
}