package com.stat.vod.system.entity.base

data class PlayerTransform(val x: Int, val y: Int, val uuid: String) {
}