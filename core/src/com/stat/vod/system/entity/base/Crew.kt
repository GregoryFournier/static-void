package com.stat.vod.system.entity.base

import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import ktx.graphics.use

/**
 * Custom implementation of a scene, it groups actors together
 */
// TODO add a way to register classes, so that when an object of that type is added/removed, it can then be added/removed to a specific list, and others
// can access that list to have a O(1) access to all members of a specific type
class Crew(val batch: SpriteBatch, private val camera: Camera = OrthographicCamera()) : Member() {
    var members = ArrayList<Member>()
    var alpha = 1f

    /**
     * Calls act on each actor
     */
    override fun act(delta: Float) {
        super.act(delta)
        for (i in 0 until members.size) {
            members.getOrNull(i)?.let { member ->
                if (member.shouldAct) {
                    member.act(delta)
                }

            }
        }
    }

    override fun draw(batch: SpriteBatch, parentAlpha: Float) {
        render()
    }

    inline fun <reified T : GameEntity> filterIsInstance(): List<T> {
        return members.filterIsInstance<T>()
    }

    inline fun <reified T : GameEntity> findById(uuid: String): T? {
        return members.filterIsInstance<T>().find { it.uuid == uuid }
    }

    fun addMember(member: Member) {
        members.add(member)
        member.crew = this
        member.onAddedToCrew(this)
        members.sortByDescending { it.drawIndex }
    }

    fun addMembers(vararg others: Member) {
        others.forEach {
            addMember(it)
        }
    }

    fun removeMember(member: Member): Boolean {
        member.onRemovedFromCrew(this)
        member.crew = null
        return members.remove(member)
    }

    fun removeMembers(vararg others: Member) {
        others.forEach {
            removeMember(it)
        }
    }

    fun removeAllMembers() {
        val temp = ArrayList(members)
        temp.forEach {
            removeMember(it)
        }
    }

    fun render() {
        camera.update()
        batch.use(camera) {
            for (i in 0 until members.size) {
                members.getOrNull(i)?.let { member ->
                    if (member.shouldDraw) {
                        member.draw(batch, alpha)
                    }
                }
            }
        }
    }
}