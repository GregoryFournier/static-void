package com.stat.vod.system.entity.base

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.NinePatch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion

sealed class Drawable {
    open var x: Float = 0f
    open var y: Float = 0f
    open var width: Float = 0f
    open var height: Float = 0f
    open var rotation: Float = 0f
    abstract fun draw(batch: SpriteBatch)
    abstract fun updateTransform(x: Float, y: Float, rot: Float)

    object None : Drawable() {
        override fun draw(batch: SpriteBatch) {
            // Do nothing
        }

        override fun updateTransform(x: Float, y: Float, rot: Float) {
            // Do Nothing
        }
    }

    /**
     * Represents a basic static image
     */
    data class StaticDrawable(val sprite: Sprite) : Drawable() {
        override var x: Float
            get() = sprite.x
            set(value) {
                sprite.x = value
            }
        override var y: Float
            get() = sprite.y
            set(value) {
                sprite.y = value
            }
        override var width: Float = sprite.width
        override var height: Float = sprite.height
        override var rotation: Float = sprite.rotation

        constructor(texture: Texture, x: Float = 0f, y: Float = 0f) : this(Sprite(texture)) {
            this.updateTransform(x, y, 0f)
        }

        override fun draw(batch: SpriteBatch) {
            sprite.draw(batch)
        }

        override fun updateTransform(x: Float, y: Float, rot: Float) {
            sprite.setPosition(x, y)
            sprite.rotation = rot
        }
    }

    /**
     * Drawable than can display/hide certain parts.
     */
    data class RegionDrawable(val region: TextureRegion) : Drawable() {
        override var x: Float = 0f
        override var y: Float = 0f
        override var width: Float = region.regionWidth.toFloat()
        override var height: Float = region.regionHeight.toFloat()

        override fun draw(batch: SpriteBatch) {
            batch.draw(region, x, y, x + width / 2, y + height / 2, width, height, 1f, 1f, rotation)
        }

        override fun updateTransform(x: Float, y: Float, rot: Float) {
            this.x = x
            this.y = y
            this.rotation = rot
        }
    }

    data class ScalableDrawable(val patch: NinePatch) : Drawable() {
        override fun draw(batch: SpriteBatch) {
            patch.draw(batch, x, y, x + width / 2, y + height / 2, width, height, 1f, 1f, rotation)
        }

        override fun updateTransform(x: Float, y: Float, rot: Float) {
            this.x = x
            this.y = y
            rotation = rot
        }
    }
}