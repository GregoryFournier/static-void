package com.stat.vod.system.entity.base

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonValue
import com.google.gson.Gson
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.engines.life.HealthChangeRequest
import com.stat.vod.system.engines.life.LifeEngine.Companion.HEALTH_CHANGE
import com.stat.vod.system.entity.Player
import com.stat.vod.system.entity.interaction.Interaction
import com.stat.vod.system.managers.GlobalFlagsManager
import com.stat.vod.system.managers.UserInfoManager
import com.stat.vod.system.utils.readValue
import com.stat.vod.system.utils.roundDown
import io.socket.client.Socket
import ktx.log.info
import java.util.*
import kotlin.collections.ArrayList
import com.stat.vod.screens.lobby.LobbyPresenter.Companion.PLAYER_TRANSFORM_UPDATE as LobbyUpdateEvent
import com.stat.vod.screens.play.PlayPresenter.Companion.PLAYER_TRANSFORM_UPDATE as PlayUpdateEvent

open class GameEntity(var uuid: String = UUID.randomUUID().toString()) : Member(), Json.Serializable {
    var velocityX: Float = 0f
    var velocityY: Float = 0f
    var currentMoveSpeed = Player.MOVE_SPEED
    var canCollide = true
    var interactable = true
    var maxHealth = DEFAULT_MAX_HEALTH
    var currentHealth = maxHealth
    var possibleInteractions = ArrayList<Interaction>()
    val defaultInteraction: Interaction?
        get() = possibleInteractions.firstOrNull()

    // Networking
    val socket: Socket = MainContext.inject()
    val userInfoManager: UserInfoManager = MainContext.inject()
    var owner: String = ""
    val ownedByMe: Boolean
        get() = owner == userInfoManager.PlayerUUID

    internal val flags: GlobalFlagsManager = MainContext.inject()

    constructor(transform: PlayerTransform) : this(transform.uuid) {
        setPosition(transform)
        owner = ""
    }

    constructor(texture: Texture) : this() {
        sprite = Drawable.StaticDrawable(Sprite(texture))
    }

    override fun write(json: Json) {
        json.writeValue("x", x)
        json.writeValue("y", y)
        json.writeValue(GameEntity::canCollide.name, canCollide)
        json.writeValue(GameEntity::maxHealth.name, maxHealth)
        json.writeValue(GameEntity::currentHealth.name, currentHealth)
        json.writeValue(GameEntity::uuid.name, uuid)
        json.writeValue(GameEntity::owner.name, owner)
        json.writeValue("class", this::class.java.name)
    }

    override fun read(json: Json, jsonData: JsonValue) {
        x = json.readValue(jsonData, "x", 0f)
        y = json.readValue(jsonData, "y", 0f)
        canCollide = json.readValue(jsonData, GameEntity::canCollide.name, canCollide)
        maxHealth = json.readValue(jsonData, GameEntity::maxHealth.name, maxHealth)
        currentHealth = json.readValue(jsonData, GameEntity::currentHealth.name, currentHealth)
        owner = json.readValue(jsonData, GameEntity::owner.name, "")
        uuid = json.readValue(jsonData, GameEntity::uuid.name, uuid)
    }

    override fun act(delta: Float) {
        applyVelocity(delta)
        super.act(delta)
        if (ownedByMe) {
            sendNetworkInformation()
        }
    }

    /**
     * @return Json String
     */
    fun getNetworkTransform(): String {
        return Gson().toJson(PlayerTransform(x.toInt(), y.toInt(), uuid))
    }

    private fun applyVelocity(delta: Float) {
        val normalizedVelocity = Vector2(velocityX, velocityY).nor()
        moveBy((delta * normalizedVelocity.x * currentMoveSpeed), (delta * normalizedVelocity.y * currentMoveSpeed))
    }

    private fun sendNetworkInformation() {
        val eventName = if (flags.isInLobby) {
            LobbyUpdateEvent
        } else {
            PlayUpdateEvent
        }
        socket.emit(eventName, getNetworkTransform())
    }

    fun isCollidingWith(other: GameEntity): Boolean {
        if (!canCollide || !other.canCollide) return false
        val thisPosition = getPhysicalRect()
        val otherPosition = other.getPhysicalRect()
        return (thisPosition.overlaps(otherPosition))
    }

    /**
     * Changes the health and sends a message to others to also change the health if notifyOthers = true
     */
    fun changeHealth(delta: Int, notifyOthers: Boolean) {
        currentHealth += delta
        if (notifyOthers) {
            socket.emit(HEALTH_CHANGE, HealthChangeRequest(uuid, delta))
        }
    }

    /**
     * returns a rectangle aligned to the tile width, normally 48x48
     */
    open fun getPhysicalRect(): Rectangle {
        return Rectangle(roundDown(x.toLong(), 48L).toFloat(),
                roundDown(y.toLong(), 48L).toFloat(),
                48f, 48f)
    }

    /**
     * Returns the rectangle in which an object'S interactable rectangle must find itself
     * if this entity can execute a local interact action.
     */
    open fun getInteractRect(): Rectangle {
        return Rectangle(x - width, y - height, width * 2, height * 2)
    }

    open fun onCollision(other: GameEntity) {
        info { "$name is colliding with ${other.name}" }
    }

    /**
     * Called when this object is interacting with other
     */
    open fun onInteractionWith(other: GameEntity, interaction: Interaction = Interaction.DEFAULT) {
        info { "$name is interacting with ${other.name}" }
    }

    /**
     * Called when other has interacted with this
     */
    open fun onInteraction(other: GameEntity, interaction: Interaction = Interaction.DEFAULT) {
        info { "No interaction configured for $name" }
    }

    /**
     * Called by screen when entity's health is below 0
     */
    open fun onDeath() {
        info { "$name/$uuid has died" }
    }

    companion object {
        const val DEFAULT_MAX_HEALTH = 999
    }
}