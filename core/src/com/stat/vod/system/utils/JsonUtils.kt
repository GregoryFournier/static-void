package com.stat.vod.system.utils

import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonValue
import com.google.gson.Gson
import ktx.json.readValue

inline fun <reified T: Any> Array<Any>.response(): T {
    return Gson().fromJson(this[0].toString(), T::class.java)
}

inline fun <reified T> Json.readValue(jsonData: JsonValue, name: String, defaultValue: T): T {
    return try {
        this.readValue(jsonData, name)
    } catch (e: Exception) {
        e.printStackTrace()
        defaultValue
    }
}