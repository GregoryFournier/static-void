package com.stat.vod.system.utils.led

import com.google.gson.annotations.SerializedName

data class GridTile(
        val coordId: Int,
        val tileId: Int,
        @SerializedName("__tileX")
        val tileX: Int,
        @SerializedName("__tileY")
        val tileY: Int
)