package com.stat.vod.system.utils.led

data class LayerDef(
        val identifier: String,
        val type: LayerType,
        val uid: Int,
        val gridSize: Int,
        val displayOpacity: Float,
        val tilesetDefUid: Int) {

    enum class LayerType {
        Tiles, Entities
    }
}