package com.stat.vod.system.utils.led

data class Level(
        val identifier: String,
        val uid: Int,
        val pxWid: Int,
        val pxHei: Int,
        val layerInstances: List<LayerInstance>) {
}