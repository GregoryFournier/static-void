package com.stat.vod.system.utils.led

import com.google.gson.annotations.SerializedName

data class LayerInstance(
        @SerializedName("__identifier")
        val identifier: String,
        @SerializedName("__type")
        val type: LayerDef.LayerType,
        val levelId: Int,
        val layerDefUid: Int,
        @SerializedName("__cWid")
        val gridWidth: Int,
        @SerializedName("__cHei")
        val gridHeight: Int,
        val gridTiles: List<GridTile>
)