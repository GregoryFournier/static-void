package com.stat.vod.system.utils.led

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.stat.vod.system.tiles.AllTilesets
import com.stat.vod.system.tiles.Tile

class LedLoader(val json: JsonObject, val tilesetManager: AllTilesets) {
    val tiles = ArrayList<Tile>()

    init {
        val gson = Gson()
        val defs: Definitions = gson.fromJson(json.get("defs"), Definitions::class.java)
//        val type = ArrayList<LayerDef>()
//        val layerDefs = gson.fromJson(defs.get("layers"), type::class.java)
        val type: Array<Level> = arrayOf()
        val levels = gson.fromJson(json.get("levels"), type::class.java)
        val map = levels.first()

        map.layerInstances.filter { it.type == LayerDef.LayerType.Tiles }.reversed().forEach { layerInstance ->
            // find the associated tileset
            val layerDef = defs.layers.first { layerDef ->
                layerDef.uid == layerInstance.layerDefUid
            }
            val tileSet = tilesetManager.getById(layerDef.tilesetDefUid)
            layerInstance.gridTiles.forEach {
                val tile = Tile(tileSet, it.tileX, it.tileY)
                var gridY = (it.coordId / layerInstance.gridWidth)
                val gridX = it.coordId - (gridY * layerInstance.gridWidth)

                gridY = layerInstance.gridHeight - gridY

                tile.setPosition(gridX * 48, gridY * 48)
                tiles.add(tile)
            }
        }


    }
}