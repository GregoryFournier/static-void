package com.stat.vod.system.utils

/** round n down to nearest multiple of m  */
fun roundDown(n: Long, m: Long): Long {
    return if (n >= 0) n / m * m else (n - m + 1) / m * m
}

/** round n up to nearest multiple of m  */
fun roundUp(n: Long, m: Long): Long {
    return if (n >= 0) (n + m - 1) / m * m else n / m * m
}