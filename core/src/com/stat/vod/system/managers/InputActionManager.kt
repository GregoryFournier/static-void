package com.stat.vod.system.managers

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import ktx.app.KtxInputAdapter
import ktx.log.info

class InputActionManager : KtxInputAdapter {
    internal val actionListeners = ArrayList<ActionListener>()
    private val disabledInputs = ArrayList<ActionListener.InputAction>()

    var inputEnabled = true

    init {
        Gdx.input.inputProcessor = this
    }

    /**
     * Latest subscribers get priority
     */
    fun subscribe(listener: ActionListener) {
        actionListeners.add(0, listener)
    }

    override fun keyDown(keycode: Int): Boolean {
        return onKey(keycode, false)
    }

    override fun keyUp(keycode: Int): Boolean {
        return onKey(keycode, true)
    }

    fun disableInputs(vararg keycodes: ActionListener.InputAction) {
        disabledInputs.addAll(keycodes.toList())
    }

    fun enableInputs(vararg keycodes: ActionListener.InputAction) {
        disabledInputs.removeAll(keycodes.toList())
    }

    fun disableAllInputs() {
        disabledInputs.addAll(ActionListener.InputAction.values())
    }

    fun enableAllInputs() {
        disabledInputs.clear()
    }

    private fun onKey(keycode: Int, isReleased: Boolean): Boolean {
        if (!inputEnabled) return false
        val action = keyboardMappings[keycode]
        if (disabledInputs.contains(action)) return false
        action?.let {
            actionListeners.forEach { listener ->
                if (isReleased) {
                    if (listener.onActionReleased(action)) return true
                } else {
                    if (listener.onAction(action)) return true
                }
            }
        } ?: info { "No Action mapped to $keycode : ${Input.Keys.toString(keycode)}" }
        return false
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        if (!inputEnabled) return false
        val action = if (button == 0) ActionListener.InputAction.CLICK else ActionListener.InputAction.RIGHT_CLICK
        if (disabledInputs.contains(action)) return false
        actionListeners.forEach { listener ->
            if (listener.onAction(action)) return true
        }
        return false
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        if (!inputEnabled) return false
        val action = if (button == 0) ActionListener.InputAction.CLICK else ActionListener.InputAction.RIGHT_CLICK
        if (disabledInputs.contains(action)) return false
        actionListeners.forEach { listener ->
            if (listener.onActionReleased(action)) return true
        }
        return false
    }

    fun unsubscribe(listener: ActionListener) {
        actionListeners.remove(listener)
    }

    private val keyboardMappings = HashMap<Int, ActionListener.InputAction>().apply {
        put(Input.Keys.W, ActionListener.InputAction.UP)
        put(Input.Keys.A, ActionListener.InputAction.LEFT)
        put(Input.Keys.S, ActionListener.InputAction.DOWN)
        put(Input.Keys.D, ActionListener.InputAction.RIGHT)
        put(Input.Keys.ENTER, ActionListener.InputAction.SELECT)
        put(Input.Keys.Z, ActionListener.InputAction.ACTIVATE)
        put(Input.Keys.ESCAPE, ActionListener.InputAction.QUIT)
        put(Input.Buttons.LEFT, ActionListener.InputAction.CLICK)
        put(Input.Keys.C, ActionListener.InputAction.SPECIAL_1)
        put(Input.Keys.SHIFT_LEFT, ActionListener.InputAction.RUN)
        put(Input.Keys.E, ActionListener.InputAction.ACTION)
    }
}

interface ActionListener {
    fun onAction(action: InputAction): Boolean { return false}
    fun onActionReleased(action: InputAction): Boolean {
        return false
    }

    enum class InputAction {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        SELECT,
        ACTIVATE,
        QUIT,
        SPECIAL_1,
        CLICK,
        RIGHT_CLICK,
        RUN,
        ACTION
    }
}
