package com.stat.vod.system.managers

interface Manager {
    fun install()
}