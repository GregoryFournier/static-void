package com.stat.vod.system.managers

import com.google.gson.Gson
import com.stat.vod.screens.base.BaseScreen
import com.stat.vod.screens.base.ScreenManager
import com.stat.vod.screens.play.model.TakeOwnershipRequest
import com.stat.vod.screens.play.model.TakeOwnershipResponse
import com.stat.vod.system.utils.response
import io.socket.client.Socket
import ktx.log.info

class OwnershipNetworkManager(private val socket: Socket, private val screenManager: ScreenManager) {
    private val ownershipCallbacks = HashMap<String, (() -> Unit)>()
    private val currentView: BaseScreen
        get() = screenManager.getCurrentScreen()

    init {
        socket.on(TAKE_OWNERSHIP_OF) { data ->
            val response = data.response<TakeOwnershipResponse>()
            if (response.granted) {
                currentView.setOwnershipOf(response.entityUUID, response.newOwnerUUID)
                ownershipCallbacks.remove(response.entityUUID)?.invoke()
            } else {
                info { "Ownership not granted for ${response.entityUUID}. Reason: ${response.error}" }
            }
        }
    }

    fun requestOwnershipOf(entityUUID: String, onOwnershipGranted: () -> Unit = {}) {
        val request = TakeOwnershipRequest(entityUUID)
        ownershipCallbacks.put(entityUUID, onOwnershipGranted)
        socket.emit(TAKE_OWNERSHIP_OF, Gson().toJson(request))
    }

    fun removeOwnershipOf(entityUUID: String, onOwnershipDropped: () -> Unit = {}) {
        val request = TakeOwnershipRequest(entityUUID)
        ownershipCallbacks.put(entityUUID, onOwnershipDropped)
        socket.emit(DROP_OWNERSHIP_OF, Gson().toJson(request))
    }

    companion object {
        private const val TAKE_OWNERSHIP_OF = "play_take_ownership"
        private const val DROP_OWNERSHIP_OF = "play_drop_ownership"
    }
}