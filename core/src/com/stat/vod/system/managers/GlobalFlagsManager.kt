package com.stat.vod.system.managers

import kotlin.properties.Delegates

class GlobalFlagsManager {
    var showCollisionWalls: Boolean by Delegates.observable(false) { property, oldValue, newValue ->
        println("Show collision walls: $newValue")
    }

    var isInLobby: Boolean by Delegates.observable(true) { property, oldValue, newValue ->
        println("Is in lobby: $newValue")
    }

    var isHost: Boolean by Delegates.observable(false) { property, oldValue, newValue ->
        println("Is Host: $newValue")
    }
}