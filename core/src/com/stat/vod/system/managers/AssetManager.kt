package com.stat.vod.system.managers

import com.badlogic.gdx.assets.AssetDescriptor
import com.stat.vod.system.context.MainContext
import ktx.freetype.registerFreeTypeFontLoaders
import org.reflections.Reflections
import com.badlogic.gdx.assets.AssetManager as GdxAssetManager

class AssetManager : Manager {
    private val assetManager: GdxAssetManager = GdxAssetManager()
    var currentlyLoadingType = ""

    init {
        install()
    }

    override fun install() {
        assetManager.registerFreeTypeFontLoaders()
        loadAllAssets()
    }

    fun update(): Boolean {
        return assetManager.update()
    }

    fun progress(): Float {
        return assetManager.progress
    }

    private fun loadAllAssets() {
        val reflection = MainContext.inject<Reflections>()
        val assets = reflection.getFieldsAnnotatedWith(Asset::class.java)
                .mapNotNull {
                    it.isAccessible = true
                    it.get(null) as? AssetDescriptor<*>
                }
                .sortedBy { it.type.name }
        assets.forEach {
            currentlyLoadingType = it.type.simpleName
            assetManager.load(it)
        }
    }

    companion object {
        fun <T> AssetDescriptor<T>.get(): T {
            return MainContext.inject<AssetManager>().assetManager.get(this)
        }
    }

}

@Target(AnnotationTarget.FIELD)
annotation class Asset()