package com.stat.vod.system.managers

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Json
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.stat.vod.screens.dev.map.MapCreationScreen
import com.stat.vod.system.collision.CollisionWall
import com.stat.vod.system.context.MainContext
import com.stat.vod.system.entity.base.Member
import com.stat.vod.system.map.PlayMap
import com.stat.vod.system.tiles.Tile
import com.stat.vod.system.utils.led.LedLoader

/**
 * Saves and loads maps
 */
class MapManager {
//    fun loadPlayMap(): PlayMap {
//        val file = Gdx.files.local(MapCreationScreen.TEMP_LEVEL_PATH)
//        val type = ArrayList<Member>()
//        val list: ArrayList<Member> = Json().fromJson(type::class.java, file.readString())
//        return PlayMap(list.filterIsInstance<Tile>(), list.filterIsInstance<CollisionWall>())
//    }

    fun loadPlayMap(): PlayMap {
        val file = Gdx.files.internal(level_1)
        val loader = LedLoader(Gson().fromJson(file.readString(), JsonObject::class.java), MainContext.inject())

        return PlayMap(loader.tiles, listOf())
    }

    companion object {
        const val TEMP_LEVEL_PATH = "data/all_setups_backup.sv"
        const val level_1 = "maps/1.json"
    }
}