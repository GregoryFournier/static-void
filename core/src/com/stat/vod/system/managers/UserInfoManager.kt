package com.stat.vod.system.managers

import com.stat.vod.system.context.MainContext
import io.socket.client.Socket
import java.util.*

/**
 * Holds information on the user, the users play session, etc.
 */
class UserInfoManager(private val socket: Socket) : Manager {
    val PlayerUUID = UUID.randomUUID().toString()

    override fun install() {

    }
}